// Copyright Epic Games, Inc. All Rights Reserved.

#include "OtusShooterGameMode.h"
#include "OtusShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AOtusShooterGameMode::AOtusShooterGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/BP_CharacterBase"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
