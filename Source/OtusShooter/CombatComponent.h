// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CombatComponent.generated.h"

#define TRACE_LENGTH 80000.f

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OTUSSHOOTER_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCombatComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	friend class ASCharacterBase;

	void EquipWeapon(class AWeapon* WeaponToEquip);

	void AttachActorToRightHand(AActor* ActorToAttach);
	void AttachActorToBackpack(AActor* ActorToAttach);
	void EquipPrimaryWeapon(AWeapon* WeaponToEquip);
	void EquipSecondaryWeapon(AWeapon* WeaponToEquip);
	void SwapWeapon();
	bool ShouldSwapWeapon();


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetAiming(bool bIsAiming);

	UFUNCTION()
	void OnRep_EquippedWeapon();

	//��������
	void FireButtonPressed(bool bPressed);

	void Fire();

	//UFUNCTION(Server, Reliable)
	//void ServerFire();

	//UFUNCTION(NetMulticast, Reliable)
	//void MulticastFire();

	//����� � ����� ������
	void TraceUnderCrosshairs(FHitResult& TraceHitResult);		

private:

	UPROPERTY()
	ASCharacterBase* Character;

	UPROPERTY(Replicated)
	class AWeapon* EquippedWeapon;

	UPROPERTY(/*Replicated*/)
	class AWeapon* SecondaryWeapon;

	UPROPERTY(ReplicatedUsing = OnRep_EquippedWeapon)
	bool bAiming;

	UPROPERTY(EditAnywhere)
	float BaseWalkSpeed;

	UPROPERTY(EditAnywhere)
	float AimWalkSpeed;

	//�������� 
	bool bFireButtonPressed;

	FVector HitTarget;

	/**
	* ������������
	*/

	float DefaultFOV;

	UPROPERTY(EditAnywhere, Category = Combat)
	float ZoomedFOV = 30.f;

	float CurrentFOV;

	UPROPERTY(EditAnywhere, Category = Combat)
	float ZoomInterpSpeed = 20.f;

	void InterpFOV(float DeltaTime);

	/**
	* Automatic fire
	*/

	FTimerHandle FireTimer;
	bool bCanFire = true;

	void StartFireTimer();

	void FireTimerFinished();



		
};
