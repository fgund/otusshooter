// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWeapon.h"

#include "Projectile.h"
#include "Engine/SkeletalMeshSocket.h"

void AProjectileWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	APawn* InstigatorPawn = Cast<APawn>(GetOwner());

	const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));

	if (ProjectileClass && InstigatorPawn)
	{
		FActorSpawnParameters SpawnParameters;

		SpawnParameters.Owner = GetOwner();
		SpawnParameters.Instigator = InstigatorPawn;

		UWorld* World = GetWorld();
		if (World)
		{
			if (MuzzleFlashSocket)
			{
				FTransform SocetTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
				FVector ToTarget = HitTarget - SocetTransform.GetLocation();
				FRotator TargetRotation = ToTarget.Rotation();

				World->SpawnActor<AProjectile>(
					ProjectileClass,
					SocetTransform.GetLocation(),
					TargetRotation,
					SpawnParameters
				);
			}
		}

	}

}
