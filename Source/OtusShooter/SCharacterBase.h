// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TurningInPlace.h"
#include "SCharacterBase.generated.h"

UCLASS()
class OTUSSHOOTER_API ASCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacterBase();

	//��������
	void PlayFireMontage(bool bAiming);
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable, Category = Movement)
	float GetMovementDirection() const;

	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	class AWeapon* OverlappingWeapon;

	UFUNCTION()
	void OnRep_OverlappingWeapon(AWeapon* LastWeapon);

	void SetOverlappingWeapon(AWeapon* Weapon);

	bool IsWeaponEquipped();

	bool IsAiming();

	FORCEINLINE float GetAO_Yaw() const { return AO_Yaw; }

	FORCEINLINE float GetAO_Pitch() const { return AO_Pitch; }

	FORCEINLINE ETurningInPlace GetTurningInPlace() const { return TurningInPlace; }

	// Zoom 
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return CameraComponent; }

	class AWeapon* GetEquippedWeapon();


protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	class USpringArmComponent* SpringArmComponent;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	class UCameraComponent* CameraComponent;

	//��������
	UPROPERTY(EditAnywhere, Category = Combat)
	class UAnimMontage* FireWeaponMontage;

	UPROPERTY(VisibleAnywhere)
	class UCombatComponent* Combat;

	float AO_Yaw;

	float AO_Pitch;

	float InterpAO_Yaw;

	FRotator StartingAimRotation;

	ETurningInPlace TurningInPlace;


	void EquipButtonPressed();

	void SwapWeaponPressed();

	void TurnInPlace(float DeltaTime);

	void AimButtonPressed();

	void AimButtonReleased();

	void AimOffset(float DeltaTime);

	//��������
	void FireButtonPressed();
	void FireButtonReleased();

private:

	// Basic movement system
	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);


};
