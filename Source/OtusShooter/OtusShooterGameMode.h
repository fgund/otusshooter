// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OtusShooterGameMode.generated.h"

UCLASS(minimalapi)
class AOtusShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOtusShooterGameMode();
};



